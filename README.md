# Firefox New Tab script

Use this script to automatically set up a local file as a new tab page
on firefox.

Based on Firefox articles found [here](https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig) and [here](https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Enterprise_deployment_before_60)

**Usage:**
```
bash <(curl -sSL https://stpg.tk/firefox-new-tab-script)
```

If you prefer to read the script first:
```
wget -O ff-set-startpage.sh https://stpg.tk/firefox-new-tab-script
bash ff-set-startpage.sh
```

Please note that this was tested _only_ on Debian and Void.
MacOS and Windows testing will take place one day...
