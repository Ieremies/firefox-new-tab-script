#!/bin/bash

# Firefox new tab override script.
#
# This script allows a user to set their new tab page
# to a custom, local file. i.e my-cool-startpage.html
#
# See: https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
#
# Usage:
#
# bash <(curl -sSL https://stpg.tk/firefox-new-tab-script) 

# Exit if non-zero status returned.
set -e

### Variables ###
firefoxDir="/usr/lib/firefox"
firefoxPrefDir="defaults/pref"
firefoxPrefDirAlt="browser/defaults/preferences"

confirm() {
  read -r -p "${1:-Do you want to continue?} [y/N] " res
  case "$res" in
    [yY][eE][sS]|[yY])
      true
      ;;
    *)
      false
      ;;
    esac
}

echo "Checking for Firefox installation folder..."
if [ ! -d ${firefoxDir} ]
then
  echo "${firefoxDir} does not exist."
  firefoxDir="/opt/firefox"
fi

if [ ! -d ${firefoxDir} ]
then
  echo "${firefoxDir} does not exist."
  firefoxDir=$(dirname $(readlink -f $(which firefox)))
fi

if [ ! -d ${firefoxDir} ]
then
  echo "Attempting to find Firefox folder using find command..."
  echo ""
  firefoxDir=$(find /usr -type d -name "firefox")
fi

if [ "$firefoxDir" == "" ] || [ ! -d ${firefoxDir} ]
then
  read -r -p "Warning: Firefox installation not found, enter path: " firefoxDir
fi

if [ "$firefoxDir" == "" ] || [ ! -d ${firefoxDir} ]
then
  echo "Error: cannot find Firefox installation!"
  exit 1
fi

echo "Found Firefox in ${firefoxDir}"
echo ""

echo "Checking for prefs folder..."
if [ -d "${firefoxDir}/${firefoxPrefDir}" ]
then
  echo "${firefoxPrefDir} exists."
  autoconfigLocation=${firefoxPrefDir}
elif [ -d "${firefoxDir}/${firefoxPrefDirAlt}" ]
then
  echo "${firefoxPrefDirAlt} exists."
  autoconfigLocation=${firefoxPrefDirAlt}
else
  echo "Error: Firefox preferences folder does not exist!"
  exit 1
fi

echo ""

if [ ! -w  "${firefoxDir}" ]
then
  echo "Current user does not have write permissions for ${firefoxDir}!"
  echo "Re-run the script as a privileged user to continue."
  exit 1
fi

if [ -f "${firefoxDir}/${autoconfigLocation}/autoconfig.js" ]
then
  echo "Warning: autoconfig.js file exists!"
  confirm "Do you want to overwrite it?"
fi

echo "Creating autoconfig.js"
echo ""
cat > "${firefoxDir}/${autoconfigLocation}/autoconfig.js" << EOL
// autoconfig.js, this line needs to be here!
pref("general.config.filename", "mozilla.cfg");
pref("general.config.obscure_value", 0);

// Enable userchrome.css customisation
pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
EOL

if [ -f "${firefoxDir}/mozilla.cfg" ]
then
  echo "Warning: mozilla.cfg file exists!"
  confirm "Do you want to overwrite it?"
fi

read -r -p "Enter path of your startpage (default: ~/.config/startpage/index.html): " startpagePath
startpagePath=${startpagePath:-${HOME}/.config/startpage/index.html}
if [ ! -f "${startpagePath}" ]
then
  echo "Error: ${startpagePath} file does not exist!"
  echo "Are you sure you entered the file path, not the URL?"
  exit 1
fi

echo "Creating mozilla.cfg with path ${startpagePath}"
echo ""
cat > "${firefoxDir}/mozilla.cfg" << EOL
// Custom firefox config

let { classes:Cc, interfaces:Ci, utils:Cu } = Components;

try {
  Cu.import("resource:///modules/AboutNewTab.jsm");
  let newTabURL = "file://${startpagePath// /%20}";
  AboutNewTab.newTabURL = newTabURL;
} catch(e) { Cu.reportError(e); }
EOL

echo "Successfully set up Firefox to use ${startpagePath} as a new tab page."
echo "Restart Firefox now."
